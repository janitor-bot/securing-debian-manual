msgid ""
msgstr ""
"Project-Id-Version: harden-doc 3.19\n"
"Report-Msgid-Bugs-To: <debian-i18n@lists.debian.org>\n"
"POT-Creation-Date: 2017-05-01 19:29+0200\n"
"PO-Revision-Date: 2018-04-24 22:15+0100\n"
"Last-Translator: Simon Brandmair <sbrandmair@gmx.net>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Securing Debian Manual"
msgstr "Debian Sicherheitshandbuch"

msgid "This document describes security in the Debian project and in the Debian operating system. Starting with the process of securing and hardening the default Debian GNU/Linux distribution installation, it also covers some of the common tasks to set up a secure network environment using Debian GNU/Linux, gives additional information on the security tools available and talks about how security is enforced in Debian by the security and audit team."
msgstr "Dieses Dokument handelt von der Sicherheit im Debian-Projekt und im Betriebssystem Debian. Es beginnt mit dem Prozess, eine Standardinstallation der Debian GNU/Linux-Distribution abzusichern und zu härten. Es behandelt auch die normalen Aufgaben, um eine sichere Netzwerkumgebung mit Debian GNU/Linux zu schaffen, und liefert zusätzliche Informationen über die verfügbaren Sicherheitswerkzeuge. Es befasst sich auch damit, wie die Sicherheit in Debian vom Sicherheits- und Auditteam gewährleistet wird. "

#~ msgid "A Guide to Securing Debian Systems"
#~ msgstr ""

