msgid ""
msgstr ""
"Project-Id-Version: harden-doc 3.19\n"
"Report-Msgid-Bugs-To: <debian-i18n@lists.debian.org>\n"
"POT-Creation-Date: 2018-05-01 10:29+0200\n"
"PO-Revision-Date: 2018-05-01 17:29+0200\n"
"Last-Translator: Oohara Yuuma <oohara@libra.interq.or.jp>\n"
"Language-Team: japanese <debian-japanese@lists.debian.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.6\n"

msgid "Automatic hardening of Debian systems"
msgstr "Debian システムを自動的に強化する"

msgid "After reading through all the information in the previous chapters you might be wondering \"I have to do quite a lot of things in order to harden my system, couldn't these things be automated?\". The answer is yes, but be careful with automated tools. Some people believe, that a hardening tool does not eliminate the need for good administration. So do not be fooled to think that you can automate the whole process and will fix all the related issues. Security is an ever-ongoing process in which the administrator must participate and cannot just stand away and let the tools do all the work since no single tool can cope with all the possible security policy implementations, all the attacks and all the environments."
msgstr "これまでの章にある情報を全部読んだら、「自分のシステムを強化するにはとても 多くのことをしなければならない、これを自動化できないか?」と思うかもしれません。 この質問への答えははいですが、自動化された道具には注意してください。 強化ツールを使ってもよい管理への必要性はなくならないと信じる人もいます。 この過程をすべて自動化して関連する問題をすべて修正できるとは考えないで ください。セキュリティは管理者も遠くにいて道具にすべての仕事をさせるわけには いかず、参加しなければならない進行中の過程です。なぜならどの道具もすべての 考えられるセキュリティポリシーの導入やすべての攻撃そしてすべての環境に対応する ことはできないからです。"

msgid "Since woody (Debian 3.0) there are two specific packages that are useful for security hardening. The <package>harden</package> package which takes an approach based on the package dependencies to quickly install valuable security packages and remove those with flaws, configuration of the packages must be done by the administrator. The <package>bastille</package> package that implements a given security policy on the local system based on previous configuration by the administrator (the building of the configuration can be a guided process done with simple yes/no questions)."
msgstr "woody (Debian 3.0) 以降にはセキュリティ強化に役立つ特定のパッケージが 2 個 あります。<application>harden</application> はパッケージの依存にもとづいてすばやく セキュリティ関連の価値あるパッケージをインストールし欠陥のあるパッケージを 削除する方法をとります。パッケージの設定は管理者が行わなければなりません。 <application>bastille</application> は管理者の以前の設定にもとづいてローカルシステムの セキュリティポリシーを導入します (設定を構築することは簡単な yes/no の 質問にそって行うこともできます)。"

msgid "Harden"
msgstr "Harden"

msgid "The <package>harden</package> package tries to make it more easy to install and administer hosts that need good security. This package should be used by people that want some quick help to enhance the security of the system. It automatically installs some tools that should enhance security in some way: intrusion detection tools, security analysis tools, etc. Harden installs the following <emphasis>virtual</emphasis> packages (i.e. no contents, just dependencies or recommendations on others):"
msgstr ""

msgid "<package>harden-tools</package>: tools to enhance system security (integrity checkers, intrusion detection, kernel patches...)"
msgstr "<application>harden-tools</application>: システムのセキュリティを高める 道具 (完全性チェッカー、侵入検知、カーネルパッチ...)"

msgid "<package>harden-environment</package>: helps configure a hardened environment (currently empty)."
msgstr "<application>harden-environment</application>: 強化された環境を設定するのを 助けます (現時点では空です)。"

msgid "<package>harden-servers</package>: removes servers considered insecure for some reason."
msgstr "<application>harden-servers</application>: 何らかの理由で危険と考えられて いるサーバを削除します。"

msgid "<package>harden-clients</package>: removes clients considered insecure for some reason."
msgstr "<application>harden-clients</application>: 何らかの理由で危険と考えられて いるクライアントを削除します。"

msgid "<package>harden-remoteaudit</package>: tools to remotely audit a system."
msgstr "<application>harden-remoteaudit</application>: リモートからシステムを監査する 道具。"

msgid "<package>harden-nids</package>: helps to install a network intrusion detection system."
msgstr ""

msgid "<package>harden-surveillance</package>: helps to install tools for monitoring of networks and services."
msgstr ""

msgid "Useful packages which are not a dependence:"
msgstr ""

msgid "<package>harden-doc</package>: provides this same manual and other security-related documentation packages."
msgstr "<application>harden-doc</application>: このマニュアルおよびその他の セキュリティ関連の文書パッケージを提供します。"

msgid "<package>harden-development</package>: development tools for creating more secure programs."
msgstr ""

msgid "Be careful because if you have software you need (and which you do not wish to uninstall for some reason) and it conflicts with some of the packages above you might not be able to fully use <package>harden</package>. The harden packages do not (directly) do a thing. They do have, however, intentional package conflicts with known non-secure packages. This way, the Debian packaging system will not approve the installation of these packages. For example, when you try to install a telnet daemon with <package>harden-servers</package>, <package>apt</package> will say:"
msgstr "必要なソフトウェアがあって (そして何らかの理由でアンインストールしたくは なくて)、それが上記のパッケージのどれかと対立していたら、 <application>harden</application> を完全に利用することはできないので注意してください。 harden パッケージは (直接には) 何もしません。しかし harden パッケージは 既知の安全でないパッケージと意図的に対立します。このようにすると、Debian の パッケージシステムはこれらのパッケージをインストールすることを承認しなく なります。たとえば、telnet デーモンを <application>harden-servers</application> と ともにインストールしようとすると、apt はこう言うでしょう:"

msgid ""
"\n"
"# apt-get install telnetd \n"
"The following packages will be REMOVED:\n"
"  harden-servers\n"
"The following NEW packages will be installed:\n"
"  telnetd \n"
"Do you want to continue? [Y/n]"
msgstr ""
"\n"
"# apt-get install telnetd \n"
"The following packages will be REMOVED:\n"
"  harden-servers\n"
"The following NEW packages will be installed:\n"
"  telnetd \n"
"Do you want to continue? [Y/n]"

msgid "This should set off some warnings in the administrator head, who should reconsider his actions."
msgstr "これは管理者の頭の中で警告を目立たせるはずですし、管理者は自分の行動を 考えなおすでしょう。"

msgid "Bastille Linux"
msgstr "Bastille Linux"

msgid "<ulink name=\"Bastille Linux\" url=\"http://bastille-linux.sourceforge.net/\" /> is an automatic hardening tool originally oriented towards the Red Hat and Mandrake Linux distributions. However, the <package>bastille</package> package provided in Debian (since woody) is patched in order to provide the same functionality for Debian GNU/Linux systems."
msgstr "<ulink url=\"http://www.bastille-linux.org\" name=\"Bastille Linux\" /> はもともと RedHat や Mandrake Linux ディストリビューション向けの自動強化ツールです。 しかし、Debian で (woody 以降) 提供される <application>bastille</application> パッケージは同じ機能を Debian GNU/Linux システムで提供するためにパッチが 当てられています。"

msgid "Bastille can be used with different frontends (all are documented in their own manpage in the Debian package) which enables the administrator to:"
msgstr "Bastille はいくつかのフロントエンドから使うことができます (Debian パッケージではどれも独自のマニュアルページで文書化されています)。これは 管理者が以下のことをするのを可能にします:"

msgid "Answer questions step by step regarding the desired security of your system (using <citerefentry><refentrytitle>InteractuveBastille</refentrytitle> <manvolnum>8</manvolnum></citerefentry>"
msgstr ""

msgid "Use a default setting for security (amongst three: Lax, Moderate or Paranoia) in a given setup (server or workstation) and let Bastille decide which security policy to implement (using <citerefentry><refentrytitle>BastilleChooser</refentrytitle> <manvolnum>8</manvolnum></citerefentry>)."
msgstr "与えられたセットアップ (サーバまたはワークステーション) における セキュリティ (Lax、Moderate または Paranoia の 3 種類から選ぶ) に 対するデフォルトの設定を使って Bastille にどのセキュリティポリシーを 導入するか決めさせる (<citerefentry><refentrytitle>BastilleChooser</refentrytitle> <manvolnum>8</manvolnum></citerefentry> を使って)"

msgid "Take a predefined configuration file (could be provided by Bastille or made by the administrator) and implement a given security policy (using <citerefentry><refentrytitle>AutomatedBastille</refentrytitle> <manvolnum>8</manvolnum></citerefentry>)."
msgstr ""
